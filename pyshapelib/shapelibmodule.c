/* Copyright (c) 2001-2008 by Intevation GmbH
* Authors:
* Bram de Greve <bram.degreve@bramz.net>
* Bernhard Herzog <bh@intevation.de>
*
* This program is free software under the LGPL (>=v2)
* Read the file COPYING coming with pyshapelib for details.
*/

#include "pyshapelib_common.h"

/* --- SHPObject ----------------------------------------------------------------------------------------------------- */

typedef struct
{
	PyObject_HEAD
	SHPObject* shpObject;
}
SHPObjectObject;

enum {
	vtXY,
	vtXYM,
	vtXYZM,
	vtInvalid
} VertexType;

int determine_vertex_type(int shape_type, int* has_z, int* has_m)
{
	switch (shape_type)
	{
	case SHPT_POINT:
	case SHPT_ARC:
	case SHPT_POLYGON:
	case SHPT_MULTIPOINT:
		if (has_z) *has_z = 0;
		if (has_m) *has_m = 0;
		return vtXY;
	case SHPT_POINTM:
	case SHPT_ARCM:
	case SHPT_POLYGONM:
	case SHPT_MULTIPOINTM:
		if (has_z) *has_z = 0;
		if (has_m) *has_m = 1;
	case SHPT_POINTZ:
	case SHPT_ARCZ:
	case SHPT_POLYGONZ:
	case SHPT_MULTIPOINTZ:
	case SHPT_MULTIPATCH:
		if (has_z) *has_z = 1;
		if (has_m) *has_m = 1;
		return vtXYZM;
	default:
		if (has_z) *has_z = 0;
		if (has_m) *has_m = 0;
		return vtInvalid;
	}
}
	
/* allocator
 */
static PyObject* shpobject_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
	SHPObjectObject* self;	
	self = (SHPObjectObject*) type->tp_alloc(type, 0);
	self->shpObject = NULL;
	return (PyObject*) self;
}

/* deallocator
 */
static void shpobject_dealloc(SHPObjectObject* self)
{
	SHPDestroyObject(self->shpObject);
	self->shpObject = NULL;
	Py_TYPE(self)->tp_free((PyObject*)self);
}

static int unpack_vertex(PyObject* vertex, int vertex_type, 
		double* xs, double* ys, double* zs, double* ms, int offset);

/* The constructor of SHPObject. parts is a list of lists of tuples
* describing the parts and their vertices just likethe output of the
* vertices() method. part_type_list is the list of part-types and may
* be NULL. For the meaning of the part-types and their default value
* see the Shaplib documentation.
*/
static int shpobject_init(SHPObjectObject* self, PyObject* args, PyObject* kwds)
{
	int type;
	int id;
	PyObject* parts = NULL; 
	PyObject* part_type_list = NULL;
	
	int num_parts;
	int num_vertices;
	int part_start;
	
	double* xs = NULL;
	double* ys = NULL;
	double* zs = NULL;
	double* ms = NULL;
	int* part_starts = NULL;
	int* part_types = NULL;
	
	PyObject* part = NULL;
	int vertex_type;
	int has_z;
	int has_m;
	
	int i;
	int return_code = -1;
	
	/* first, unpack parameters */
	if (kwds != NULL && PyDict_Size(kwds) > 0)
	{
		PyErr_Format(PyExc_TypeError, "shapelib.SHPObject.__init__ takes no keyword arguments");
		return -1;
	}
	if (!PyArg_ParseTuple(args, "iiO|O:__init__", &type, &id, &parts, &part_type_list)) return -1;

	/* check parts */
	if (!PySequence_Check(parts))
	{
		PyErr_SetString(PyExc_TypeError, "parts is not a sequence");
		return -1;
	}
	num_parts = PySequence_Length(parts);
	if (num_parts < 0)
	{
		PyErr_SetString(PyExc_TypeError, "cannot determine length of parts");
		return -1;
	}
	
	/* parts and part_types have to have the same lengths */
	if (part_type_list == Py_None)
	{
		part_type_list = NULL;
	}
	if (part_type_list)
	{
		if (!PySequence_Check(parts))
		{
			PyErr_SetString(PyExc_TypeError, "part_type_list is not a sequence");
			return -1;
		}
		if (PySequence_Length(part_type_list) != num_parts)
		{
			PyErr_SetString(PyExc_TypeError, "parts and part_types have to have the same lengths");
			return -1;
		}
	}

	/* determine how many vertices there are altogether */
	num_vertices = 0;
	for (i = 0; i < num_parts; ++i)
	{
		PyObject* part = PySequence_ITEM(parts, i);
		if (!PySequence_Check(part))
		{
			PyErr_SetString(PyExc_TypeError, "at least one item in parts is not a sequence");
			Py_DECREF(part);
			return -1;
		}
		num_vertices += PySequence_Length(part);
		Py_DECREF(part);
	}
	
	
	vertex_type = determine_vertex_type(type, &has_z, &has_m);

	/* allocate the memory for the various arrays and check for memory errors */
	xs = malloc(num_vertices * sizeof(double));
	ys = malloc(num_vertices * sizeof(double));
	zs = has_z ? malloc(num_vertices * sizeof(double)) : NULL;
	ms = has_m ? malloc(num_vertices * sizeof(double)) : NULL;
	part_starts = malloc(num_parts * sizeof(int));
	part_types = part_type_list ? malloc(num_parts * sizeof(int)) : 0;

	if (!xs || !ys || (has_z && !zs) || (has_m && !ms) || !part_starts || (part_type_list && !part_types))
	{
		PyErr_NoMemory();
		goto exit;
	}

	/* convert the part types */
	if (part_type_list)
	{
		for (i = 0; i < num_parts; i++)
		{
			PyObject* otype = PySequence_ITEM(part_type_list, i);
			part_types[i] = PYSHAPELIB_ASLONG(otype);
			Py_DECREF(otype);
			if (part_types[i] < 0)
			{
				PyErr_SetString(PyExc_TypeError, "at least one item in part_type_list is not an integer or is negative");
				goto exit;
			}
		}
	}

	/* convert the list of parts */
	part_start = 0;
	for (i = 0; i < num_parts; ++i)
	{
		int j, length;
		
		part = PySequence_ITEM(parts, i);
		length = PySequence_Length(part);
		if (length < 0) goto exit;
		part_starts[i] = part_start;

		for (j = 0; j < length; ++j)
		{
			PyObject* vertex = PySequence_ITEM(part, j);
			if (!unpack_vertex(vertex, vertex_type, xs, ys, zs, ms, part_start + j))
			{
				Py_DECREF(vertex);
				PyErr_SetString(PyExc_TypeError, "at least one vertex is of the wrong format");
				goto exit;
			}
			Py_DECREF(vertex);
		}
		Py_DECREF(part);
		part = NULL;
		part_start += length;
	}

	self->shpObject = SHPCreateObject(type, id, num_parts, part_starts, part_types, num_vertices, xs, ys, zs, ms);
	return_code = 0;
	
exit:
	Py_XDECREF(part);
	free(xs);
	free(ys);
	free(zs);
	free(ms);
	free(part_starts);
	free(part_types);
	return return_code;
}

/* helper for shpobject_init. Unpacks vertices 
 */
static int unpack_vertex(PyObject* vertex, int vertex_type, 
		double* xs, double* ys, double* zs, double* ms, int offset)
{
	int ok;
	PyObject* m_object;
	PyObject *err_type, *err_value, *err_traceback;

	switch (vertex_type)
	{
	case vtXY:
		return PyArg_ParseTuple(vertex, "dd:__init__", xs + offset, ys + offset);

	case vtXYM:
		ms[offset] = PYSHAPELIB_NO_DATA;
		ok = PyArg_ParseTuple(vertex, "dd|d:__init__", xs + offset, ys + offset, ms + offset);
		if (!ok)
		{
			/* maybe they specified None as M value */
			PyErr_Fetch(&err_type, &err_value, &err_traceback);
			ok = PyArg_ParseTuple(vertex, "ddO:__init__", xs + offset, ys + offset, &m_object);
			if (ok && m_object == Py_None)
			{
				Py_XDECREF(err_type);
				Py_XDECREF(err_value);
				Py_XDECREF(err_traceback);
			}
			else
			{
				PyErr_Restore(err_type, err_value, err_traceback);
			}
		}
		return ok;

	case vtXYZM:
		zs[offset] = 0.;
		ms[offset] = PYSHAPELIB_NO_DATA;
		ok = PyArg_ParseTuple(vertex, "dd|dd:__init__", xs + offset, ys + offset,
				zs + offset, ms + offset);
		if (!ok)
		{
			/* maybe they specified None as M value */
			PyErr_Fetch(&err_type, &err_value, &err_traceback);
			ok = PyArg_ParseTuple(vertex, "dddO:__init__", xs + offset, ys + offset, 
					zs + offset, &m_object);
			if (ok && m_object == Py_None)
			{
				Py_XDECREF(err_type);
				Py_XDECREF(err_value);
				Py_XDECREF(err_traceback);
			}
			else
			{
				PyErr_Restore(err_type, err_value, err_traceback);
			}
		}
		return ok;

	default:
		PyErr_SetString(PyExc_NotImplementedError, "vertex type not implemented");
		return 0;
	}
}

/*
* The extents() method of SHPObject.
*
* Return the extents as a tuple of two 4-element lists with the min.
* and max. values of x, y, z, m.
*/
static PyObject* shpobject_extents(SHPObjectObject* self)
{
	SHPObject* object = self->shpObject;
	return Py_BuildValue("(dddd)(dddd)",
			object->dfXMin, object->dfYMin, object->dfZMin, object->dfMMin, 
			object->dfXMax, object->dfYMax, object->dfZMax, object->dfMMax);
}


/*
* The vertices() method of SHPObject.
*
* Return the x and y coords of the vertices as a list of lists of
* tuples.
*/

static PyObject* build_vertex_list(SHPObject *object, int index, int length, int vertex_type);

static PyObject* shpobject_vertices(SHPObjectObject* self)
{
	PyObject *result = NULL;
	PyObject *part = NULL;
	int part_idx, vertex_idx;
	int length = 0;
	int vertex_type;
	
	SHPObject* object = self->shpObject;
	vertex_type = determine_vertex_type(object->nSHPType, NULL, NULL);

	if (object->nParts > 0)
	{
		/* A multipart shape. Usual for SHPT_ARC and SHPT_POLYGON */
	
		result = PyList_New(object->nParts);
		if (!result)
			return NULL;

		for (part_idx = 0, vertex_idx = 0; part_idx < object->nParts; part_idx++)
		{
			if (part_idx < object->nParts - 1)
				length = (object->panPartStart[part_idx + 1]
					- object->panPartStart[part_idx]);
			else
				length = object->nVertices - object->panPartStart[part_idx];
			
			part = build_vertex_list(object, vertex_idx, length, vertex_type);
			if (!part) goto fail;

			if (PyList_SetItem(result, part_idx, part) < 0) goto fail;

			vertex_idx += length;
		}
	}
	else
	{
		/* only one part. usual for SHPT_POINT */
		result = build_vertex_list(object, 0, object->nVertices, vertex_type);
	}

	return result;

fail:
	Py_XDECREF(part);
	Py_DECREF(result);
	return NULL;
}


/* Return the length coordinates of the shape object starting at vertex
* index as a Python-list of tuples. Helper function for
* SHPObject_vertices.
*/
static PyObject* build_vertex_list(SHPObject *object, int index, int length, int vertex_type)
{
	int i;
	PyObject * list;
	PyObject * vertex = NULL;

	list = PyList_New(length);
	if (!list)
		return NULL;

	for (i = 0; i < length; i++, index++)
	{
		switch (vertex_type)
		{
		case vtXY:
			vertex = Py_BuildValue("dd", object->padfX[index], object->padfY[index]);
			break;			
		case vtXYM:
			vertex = Py_BuildValue("ddd", object->padfX[index], object->padfY[index], 
				object->padfM[index]);
			break;
		case vtXYZM:
			vertex = Py_BuildValue("dddd", object->padfX[index], object->padfY[index], 
				object->padfZ[index], object->padfM[index]);
			break;			
		default:
			goto fail;
		}

		if (!vertex || PyList_SetItem(list, i, vertex) < 0) goto fail;
	}
	
	return list;

fail:
	Py_DECREF(list);
	return NULL;
}



static PyObject* shpobject_part_types(SHPObjectObject* self)
{
	int i;
	PyObject* result = NULL;
	SHPObject* object = self->shpObject;
	
	if (object->nParts == 0 || object->panPartType == 0)
	{
		Py_RETURN_NONE;
	}
	
	result = PyTuple_New(object->nParts);
	if (!result) return NULL;
	
	for (i = 0; i < object->nParts; ++i)
	{
		/* PyTuple_SetItem steals a reference */
		PyObject* part_type = PYSHAPELIB_FROMLONG((long)object->panPartType[i]);
		if (!part_type || PyTuple_SetItem(result, i, part_type) < 0) goto fail;
	}	
	return result;
	
fail:
	Py_DECREF(result);
	return NULL;
}



static PyObject* shpobject_type(SHPObjectObject* self, void* closure)
{
	return PYSHAPELIB_FROMLONG(self->shpObject->nSHPType);
}



static PyObject* shpobject_id(SHPObjectObject* self, void* closure)
{
	return PYSHAPELIB_FROMLONG(self->shpObject->nShapeId);
}



static PyObject* getstate(SHPObjectObject* self)
{
	/* shpobject_vertices doesn't really return a good parts list, 
	 * as the outer layer of the onion is stripped in case of point shapes (when nParts == 0). 
	 * put that layer back! 
	 */
	PyObject* parts = NULL;
	parts = shpobject_vertices(self);
	if (parts && self->shpObject->nParts == 0)
	{
		parts = Py_BuildValue("[N]", parts);
	}
	if (!parts)
	{
		return NULL;
	}

	return Py_BuildValue("iiNN", 
		self->shpObject->nSHPType, 
		self->shpObject->nShapeId,
		parts, 
		shpobject_part_types(self));
}



/* return a string that can be feeded to eval() to reconstruct the object,
 * assuming a proper context
 */
static PyObject* shpobject_repr(SHPObjectObject* self)
{
	PyObject* format = NULL;
	PyObject* args = NULL;
	PyObject* result = NULL;
	
	format = PyUnicode_FromString("shapelib.SHPObject(%i, %i, %s, %s)");
	if (!format) return NULL;

	args = getstate(self);
	if (!args) 
	{
		Py_DECREF(format);
		return NULL;
	}
	
	result = PYSHAPELIB_FORMAT(format, args);
	Py_DECREF(args);
	Py_DECREF(format);
	return result;
}



static PyObject* shpobject_reduce(SHPObjectObject* self)
{
	return Py_BuildValue("ON",
		Py_TYPE(self),
		getstate(self));
}



static struct PyMethodDef shpobject_methods[] = 
{
	{"extents", (PyCFunction)shpobject_extents, METH_NOARGS, 
		"extents() -> ((x_min, y_min, z_min, m_min), (x_max, y_max, z_max, m_max))\n\n"
		"returns the 4D bounding box of the SHPObject"},
	{"vertices", (PyCFunction)shpobject_vertices, METH_NOARGS, 
		"vertices() ->  [[(x, y, ...), ...], ...]\n\n"
		"Returns a list of object parts, where each part is again a list of vertices. "
		"Each vertex is a tuple of two to four doubles, depending on the object type."},
	{"part_types", (PyCFunction)shpobject_part_types, METH_NOARGS, 
		"part_types() -> tuple\n\n"
		"returns a tuple of integers, each integer indicating the type of the "
		"corresponding part in vertices()"},
	{"__reduce__", (PyCFunction)shpobject_reduce, METH_NOARGS, 
		"__reduce__() -> (cls, state)"},
	{NULL}
};

static struct PyGetSetDef shpobject_getsetters[] = 
{
	{"type", (getter)shpobject_type, NULL, "type of the object (read-only)" },
	{"id", (getter)shpobject_id, NULL, "id of the object (read-only)" },
	{NULL}
};

static PyTypeObject SHPObjectType = PYSHAPELIB_DEFINE_TYPE(SHPObjectObject, shpobject, "shapelib.SHPObject", 0);


/* --- ShapeFile ----------------------------------------------------------------------------------------------------- */

typedef struct
{
	PyObject_HEAD
	SHPHandle handle;
}
ShapeFileObject;

/* allocator 
 */
static PyObject* shapefile_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
	ShapeFileObject* self;	
	self = (ShapeFileObject*) type->tp_alloc(type, 0);
	self->handle = NULL;
	return (PyObject*) self;
}

/* destructor
*/
static void shapefile_dealloc(ShapeFileObject* self)
{
	SHPClose(self->handle);
	Py_TYPE(self)->tp_free((PyObject*)self);
}

/* constructor
 */
static int shapefile_init(ShapeFileObject* self, PyObject* args, PyObject* kwds)
{
	SAHooks hooks;
	char* file = NULL;
	char* mode = "rb";
	static char *kwlist[] = {"name", "mode", NULL};

	SHPClose(self->handle);
	self->handle = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kwds, "et|s:ShapeFile", kwlist, 
		PYSHAPELIB_FILENAME_ENCODING, &file, &mode)) return -1;	
		
	PYSHAPELIB_SETUPHOOKS(&hooks);
	self->handle = SHPOpenLL(file, mode, &hooks);
	if (!self->handle)
	{
		PyErr_SetFromErrnoWithFilename(PyExc_IOError, file);
		PyMem_Free(file);
		return -1;
	}
	PyMem_Free(file);

	return 0;
}



static PyObject* shapefile_close(ShapeFileObject* self)
{
	SHPClose(self->handle);
	self->handle = NULL;
	Py_RETURN_NONE;
}



static PyObject* shapefile_info(ShapeFileObject* self)
{
	SHPHandle handle = self->handle;
	return Py_BuildValue("ii(dddd)(dddd)",
			handle->nRecords, handle->nShapeType,
			handle->adBoundsMin[0], handle->adBoundsMin[1], handle->adBoundsMin[2], handle->adBoundsMin[3],
			handle->adBoundsMax[0], handle->adBoundsMax[1], handle->adBoundsMax[2], handle->adBoundsMax[3]);
}



static PyObject* shapefile_read_object(ShapeFileObject* self, PyObject* args)
{
	int index;
	SHPObject* object;
	SHPObjectObject* result;
	
	if (!PyArg_ParseTuple(args, "i:read_object", &index)) return NULL;
	
	object = SHPReadObject(self->handle, index);	
	if (!object)
	{
		PyErr_SetString(PyExc_RuntimeError, "failed to read object");
		return NULL;
	}
	
	result = PyObject_New(SHPObjectObject, &SHPObjectType);
	if (!result)
	{
		return PyErr_NoMemory();
	}
	
	result->shpObject = object;
	return (PyObject*) result;
}



static PyObject* shapefile_write_object(ShapeFileObject* self, PyObject* args)
{
	int index, result;
	PyObject* object;
	
	if (!PyArg_ParseTuple(args, "iO!:write_object", &index, &SHPObjectType, &object)) return NULL;
	
	result = SHPWriteObject(self->handle, index, ((SHPObjectObject*)object)->shpObject);
	if (result < 0)
	{
		PyErr_SetString(PyExc_RuntimeError, "failed to write object");
		return NULL;
	}
	return PYSHAPELIB_FROMLONG((long)result);
}

static PyObject* shapefile_cobject(ShapeFileObject* self)
{
	return PYSHAPELIB_FROMVOID(self->handle, NULL, NULL);
}

static PyObject* shapefile_repr(ShapeFileObject* self)
{
	/* TODO: it would be nice to do something like "shapelib.ShapeFile(filename, mode)" instead */
	return PYSHAPELIB_FROMFORMAT("<shapelib.ShapeFile object at %p>", self->handle);
}

static struct PyMethodDef shapefile_methods[] = 
{
	{"close", (PyCFunction)shapefile_close, METH_NOARGS, 
		"close() -> None\n\n"
		"close the shape file" },
	{"info", (PyCFunction)shapefile_info, METH_NOARGS,
		"info() -> (num_shapes, type, (x_min, y_min, z_min, m_min), (x_max, y_max, z_max, m_max))\n\n"
		"returns info about ShapeFile with:\n"
		"- num_shapes: the number of the objects in the file\n"
		"- type: the type of the shape file (SHPT_POINT, SHPT_POLYGON, ...)\n"
		"- (x_min, ...), (x_max, ...): 4D bounding box of the data in the shape file" },
	{"read_object", (PyCFunction)shapefile_read_object, METH_VARARGS, 
		"read_object(id) -> SHPObject\n\n"
		"Returns shape indexed by id" },
	{"write_object", (PyCFunction)shapefile_write_object, METH_VARARGS, 
		"write_object(id, object) -> id\n\n"
		"Write an object at index id, and returns id."
		"If id == -1, the object is appended at the end of the shape file."},
	{"cobject", (PyCFunction)shapefile_cobject, METH_NOARGS, 
		"cobject() -> CObject\n\n"
		"Return the shapelib SHPHandle as a Python CObject"},
	{NULL}
};

static struct PyGetSetDef shapefile_getsetters[] = 
{
	{NULL}
};

static PyTypeObject ShapeFileType = PYSHAPELIB_DEFINE_TYPE(ShapeFileObject, shapefile, "shapelib.ShapeFile", 0);

/* --- shapelib ------------------------------------------------------------------------------------------------------ */

static PyObject* shapelib_open(PyObject* module, PyObject* args)
{
	return PyObject_CallObject((PyObject*)&ShapeFileType, args);
}

static PyObject* shapelib_create(PyObject* module, PyObject* args)
{
	SAHooks hooks;
	char* file;
	int type;
	ShapeFileObject* result;
	SHPHandle handle = NULL;

	if (!PyArg_ParseTuple(args, "eti:create", PYSHAPELIB_FILENAME_ENCODING, &file, &type)) return NULL;
	
	PYSHAPELIB_SETUPHOOKS(&hooks);
	handle = SHPCreateLL(file, type, &hooks);
	if (!handle)
	{
			PyErr_SetFromErrnoWithFilename(PyExc_IOError, file);
			PyMem_Free(file);
			return NULL;
	}
	PyMem_Free(file);

	result = PyObject_New(ShapeFileObject, &ShapeFileType);
	if (!result)
	{
		SHPClose(handle);
		return PyErr_NoMemory();
	}
	
	result->handle = handle;
	return (PyObject*) result;
}
	
static PyShapeLibAPI shapelib_the_api = 
{
	SHPReadObject,
	SHPDestroyObject,
	SHPCreateTree,
	SHPDestroyTree,
	SHPTreeFindLikelyShapes
};

static PyObject* shapelib_c_api(PyObject* module) 
{
	return PYSHAPELIB_FROMVOID(&shapelib_the_api, NULL, NULL);
}

static PyObject* shapelib_type_name(PyObject* module, PyObject* args)
{
	int type;
	if (!PyArg_ParseTuple(args, "i:type_name", &type)) return NULL;
	return PYSHAPELIB_FROMSTRING(SHPTypeName(type));
}

static PyObject* shapelib_part_type_name(PyObject* module, PyObject* args)
{
	int type;
	if (!PyArg_ParseTuple(args, "i:part_type_name", &type)) return NULL;
	return PYSHAPELIB_FROMSTRING(SHPPartTypeName(type));
}

static struct PyMethodDef shapelib_methods[] = 
{
	{"open", (PyCFunction)shapelib_open, METH_VARARGS, 
		"open(name [, mode='rb']) -> ShapeFile\n\n"
		"opens a ShapeFile" },
	{"create", (PyCFunction)shapelib_create, METH_VARARGS, 
		"create(name, type) -> ShapeFile\n\n"
		"creates a ShapeFile of a certain type (one of SHPT_POINT, SHPT_POLYGON)" },
	{"c_api", (PyCFunction)shapelib_c_api, METH_NOARGS, 
		"c_api() -> CObject\n\n"
		"get C API of shapelib as a CObject" },
	{"type_name", (PyCFunction)shapelib_type_name, METH_VARARGS, 
		"type_name(type) -> string\n\n"
		"return type as string" },
	{"part_type_name", (PyCFunction)shapelib_part_type_name, METH_VARARGS, 
		"part_type_name(part_type) -> string\n\n"
		"return part type as string" },
	{NULL}
};

#if PYSHAPELIB_IS_PY3K

static struct PyModuleDef shapelib_moduledef = {
		PyModuleDef_HEAD_INIT,
		"shapelib",
		NULL,
		0,
		shapelib_methods,
		NULL,
		NULL,
		NULL,
		NULL
};

PyMODINIT_FUNC PyInit_shapelib(void)
#else
PyMODINIT_FUNC initshapelib(void)
#endif
{
#if PYSHAPELIB_IS_PY3K
	PyObject *module = PyModule_Create(&shapelib_moduledef);
#else
	PyObject* module = Py_InitModule("shapelib", shapelib_methods);
#endif
	if (!module) PYSHAPELIB_INITRETURN(NULL);
	
	PYSHAPELIB_ADD_TYPE(SHPObjectType, "SHPObject");
	PYSHAPELIB_ADD_TYPE(ShapeFileType, "ShapeFile");
	
	PyModule_AddIntConstant(module, "_have_utf8_hooks", HAVE_UTF8_HOOKS);

	PYSHAPELIB_ADD_CONSTANT(SHPT_NULL);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POINT);
	PYSHAPELIB_ADD_CONSTANT(SHPT_ARC);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POLYGON);
	PYSHAPELIB_ADD_CONSTANT(SHPT_MULTIPOINT);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POINTZ);
	PYSHAPELIB_ADD_CONSTANT(SHPT_ARCZ);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POLYGONZ);
	PYSHAPELIB_ADD_CONSTANT(SHPT_MULTIPOINTZ);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POINTM);
	PYSHAPELIB_ADD_CONSTANT(SHPT_ARCM);
	PYSHAPELIB_ADD_CONSTANT(SHPT_POLYGONM);
	PYSHAPELIB_ADD_CONSTANT(SHPT_MULTIPOINTM);
	PYSHAPELIB_ADD_CONSTANT(SHPT_MULTIPATCH);
	PYSHAPELIB_ADD_CONSTANT(SHPP_TRISTRIP);
	PYSHAPELIB_ADD_CONSTANT(SHPP_TRIFAN);
	PYSHAPELIB_ADD_CONSTANT(SHPP_OUTERRING);
	PYSHAPELIB_ADD_CONSTANT(SHPP_INNERRING);
	PYSHAPELIB_ADD_CONSTANT(SHPP_FIRSTRING);
	PYSHAPELIB_ADD_CONSTANT(SHPP_RING);
	
	PYSHAPELIB_INITRETURN(module);
}

