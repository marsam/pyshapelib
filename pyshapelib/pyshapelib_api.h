/* Header file for the PyShapelib API for other Python modules */
/* $Revision$ */

#ifndef PYSHAPELIB_API_H
#define PYSHAPELIB_API_H

#if PY_VERSION_HEX < 0x03010000 // < 3.1
#	define PYSHAPELIB_CAPSULE_TYPE PyCObject_Type
#	define PYSHAPELIB_FROMVOID(value, name, destructor) PyCObject_FromVoidPtr(value, destructor)
#	define PYSHAPELIB_ASVOID(object, name) PyCObject_AsVoidPtr(object)
#else
#	define PYSHAPELIB_CAPSULE_TYPE PyCapsule_Type
#	define PYSHAPELIB_FROMVOID(value, name, destructor) PyCapsule_New(value, name, destructor)
#	define PYSHAPELIB_ASVOID(object, name) PyCapsule_GetPointer(object, name)
#endif


typedef struct {
    /* Shapefile functions */
    SHPObject * (*SHPReadObject)(SHPHandle hSHP, int iShape);
    void (*SHPDestroyObject)(SHPObject * psObject);

    /* SHPTree functions */
    SHPTree * (*SHPCreateTree)(SHPHandle hSHP, int nDimension, int nMaxDepth,
			       double *padfBoundsMin, double *padfBoundsMax);
    void (*SHPDestroyTree)(SHPTree * hTree);
    int * (*SHPTreeFindLikelyShapes)(SHPTree * hTree, double * padfBoundsMin,
				     double * padfBoundsMax, int *);
} PyShapeLibAPI;


/* Macro to import the shapelib module, extract the API pointer and
 * assign it to the variable given as argument */
#define PYSHAPELIB_IMPORT_API(apivariable)				   \
{									   \
    PyObject * shapelib = PyImport_ImportModule("shapelib");		   \
    if (shapelib)							   \
    {									   \
	PyObject * c_api_func = PyObject_GetAttrString(shapelib, "c_api"); \
	if (c_api_func)							   \
	{								   \
	    PyObject * cobj = PyObject_CallObject(c_api_func, NULL);	   \
	    if (cobj)							   \
	    {								   \
		(apivariable) = (PyShapeLibAPI*)PYSHAPELIB_ASVOID(cobj, NULL); \
	    }								   \
	}								   \
    }									   \
}


#endif /* PYSHAPELIB_API_H */
