/* Copyright (c) 2001, 2002, 2007 by Intevation GmbH
 * Authors:
 * Bernhard Herzog <bh@intevation.de>
 *
 * This program is free software under the LGPL (>=v2)
 * Read the file COPYING coming with pyshapelib for details.
 */

/* Python wrapper for the shapelib SHPTree */

#include "pyshapelib_common.h"
#include <shapefil.h>

PyShapeLibAPI * api;

typedef struct {
	PyObject_HEAD
	SHPTree * tree; 
} SHPTreeObject;

extern PyTypeObject SHPTreeType;

#define SHPTree_Check(v)		((v)->ob_type == &SHPTreeType)

/* Create a new python wrapper object from a SHPTree pointer */
static PyObject *
SHPTreeObject_FromSHPTree(SHPTree* tree)
{
	SHPTreeObject * self = PyObject_NEW(SHPTreeObject, &SHPTreeType);
	if (!self)
		return NULL;

	self->tree = tree;

	return (PyObject *)self;
}

/* Deallocate the SHPTree wrapper. */
static void
shptree_dealloc(SHPTreeObject * self)
{
	api->SHPDestroyTree(self->tree);
	PyObject_Del(self);
}

/* Return the repr of the wrapper */
static PyObject *
shptree_repr(SHPTreeObject * self)
{
	return PYSHAPELIB_FROMFORMAT("<shptree.SHPTree object at %p>", self->tree);
}

static PyObject *
shptree_find_shapes(SHPTreeObject * self, PyObject * args)
{
	double min[4] = {0, 0, 0, 0};
	double max[4] = {0, 0, 0, 0};
	int count, idx;
	int * ids;
	PyObject * list = NULL, *temp = NULL;

	if (!PyArg_ParseTuple(args, "(dd)(dd)", min + 0, min + 1,
			  max + 0, max + 1))
	return NULL;

	ids = api->SHPTreeFindLikelyShapes(self->tree, min, max, &count);

	list = PyList_New(count);
	if (!list)
	goto fail;

	/* Turn the returned array of indices into a python list of ints. */
	for (idx = 0; idx < count; idx++)
	{
	temp = PYSHAPELIB_FROMLONG(ids[idx]);
	if (!temp)
		goto fail;

	if (PyList_SetItem(list, idx, temp) == -1)
	{
		/* temp's refcount has already be decreased. Set temp to
		 * NULL so that the fail code doesn't do it again
		 */
		temp = NULL;
		goto fail;
	}
	}

	free(ids);
	return list;
	
 fail:
	free(ids);
	Py_XDECREF(list);
	Py_XDECREF(temp);
	return NULL;
}


static struct PyMethodDef shptree_methods[] = {
	{"find_shapes",	(PyCFunction)shptree_find_shapes,	METH_VARARGS},
	{NULL,	NULL}
};


PyTypeObject SHPTreeType =
{
	PYSHAPELIB_HEAD_INIT(NULL, 0)
	"SHPTree",							/*tp_name*/
	sizeof(SHPTreeObject),				/*tp_basicsize*/
	0,									/*tp_itemsize*/
	(destructor) shptree_dealloc,		/*tp_dealloc*/
	0,									/*tp_print*/
	0,									/*tp_getattr*/
	0,									/*tp_setattr*/
	0,									/*tp_compare*/
	(reprfunc) shptree_repr,			/*tp_repr*/
	0,									/*tp_as_number*/
	0,									/*tp_as_sequence*/
	0,									/*tp_as_mapping*/
	0,									/*tp_hash */
	0,									/*tp_call*/
	0,									/*tp_str*/
	0,									/*tp_getattro*/
	0,									/*tp_setattro*/
	0,									/*tp_as_buffer*/
	Py_TPFLAGS_DEFAULT,					/*tp_flags*/
	0,									/* tp_doc */
	0,									/* tp_traverse */
	0,									/* tp_clear */
	0,									/* tp_richcompare */
	0,									/* tp_weaklistoffset */
	0,									/* tp_iter */
	0,									/* tp_iternext */
	shptree_methods,					/* tp_methods */
	0,									/* tp_members */
	0,									/* tp_getset */
	0,									/* tp_base */
	0,									/* tp_dict */
	0,									/* tp_descr_get */
	0,									/* tp_descr_set */
	0,									/* tp_dictoffset */
	0,									/* tp_init */
	0,									/* tp_alloc */
	0,									/* tp_new */
};


static PyObject *
shptree_from_shapefile(PyObject * self, PyObject * args)
{
	SHPTree * tree;
	SHPHandle handle;
	PyObject * cobject;
	int dimension, max_depth;

	if (!PyArg_ParseTuple(args, "O!ii", &PYSHAPELIB_CAPSULE_TYPE, &cobject,
			  &dimension, &max_depth))
	return NULL;

	handle = PYSHAPELIB_ASVOID(cobject, NULL);

	tree = api->SHPCreateTree(handle, dimension, max_depth, NULL, NULL);

	/* apparently SHPCreateTree doesn't do any error checking, so we
	 * have to assume that tree is valid at this point. */
	return SHPTreeObject_FromSHPTree(tree);
}


static PyMethodDef module_functions[] = {
	{"SHPTree",		shptree_from_shapefile,		METH_VARARGS},
	{ NULL, NULL }
};


#if PYSHAPELIB_IS_PY3K

static struct PyModuleDef shptree_moduledef = {
		PyModuleDef_HEAD_INIT,
		"shptree",
		NULL,
		0,
		shptree_methods,
		NULL,
		NULL,
		NULL,
		NULL
};

PyMODINIT_FUNC PyInit_shptree(void)
#else
PyMODINIT_FUNC initshptree(void)
#endif
{
#if PYSHAPELIB_IS_PY3K
	PyObject *module = PyModule_Create(&shptree_moduledef);
#else
	PyObject* module = Py_InitModule("shptree", shptree_methods);
#endif
	if (!module) PYSHAPELIB_INITRETURN(NULL);

	PYSHAPELIB_ADD_TYPE(SHPTreeType, "SHPTree");
	PYSHAPELIB_IMPORT_API(api);
}
