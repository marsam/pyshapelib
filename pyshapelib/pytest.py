import shapelib, dbflib, shptree

filename = "testfile"
# filename = u"x\u03C0\u03C1\u03C2" # test a unicode filename

#
#       The the shapefile module
#

def test_shpobject(obj):
    # The vertices method returns the shape as a list of lists of tuples.
    print "vertices:", obj.vertices()
    
    # The part_types method returns a tuple with the types of every part
    print "part_types:", obj.part_types()

    # The extents returns a tuple with two 4-element lists with the min.
    # and max. values of the vertices.
    print "extents:", obj.extents()

    # The type attribute is the type code (one of the SHPT* constants
    # defined in the shapelib module)
    print "type:", obj.type

    # The id attribute is the shape id
    print "id:", obj.id
    
    # the __repr__ method returns a string that can be eval()'ed to 
    # recreate the object.  This __repr__ is also used by __str__
    # and print
    print "obj:", obj
    print "reconstruction using __repr__:",
    obj_repr = repr(obj)
    obj_copy = eval(obj_repr)
    if repr(obj_copy) == obj_repr:
        print "ok"
    else:
        print "failed"
    

    
def make_shapefile(filename):
    print "\n* Creating a ShapeFile"
    
    # Create a shapefile with polygons
    outfile = shapelib.create(filename, shapelib.SHPT_POLYGON)

    # Create one very simple polygon and write it to the shapefile.  The
    # vertices should be given in clockwise order to comply with the
    # shapefile specification.
    print "\nA very simple polygon"
    obj = shapelib.SHPObject(shapelib.SHPT_POLYGON, 1,
                             [[(10, 10), (10, 20), (20, 20), (10, 10)]])
    test_shpobject(obj)
    outfile.write_object(-1, obj)

    # Create a polygon with a hole.  Note that according to the
    # shapefile specification, the vertices of the outer ring have to be
    # in clockwise order and the inner rings have to be in counter
    # clockwise order.
    #
    # There's an optional fourth parameter which when given must be a
    # list of part types, one for each part of the shape.  For polygons,
    # the part type is always shapelib.SHPP_RING, though.  The part
    # types are only relevant for SHPT_MULTIPATCH shapefiles.
    print "\nPolygon with a hole"
    obj = shapelib.SHPObject(shapelib.SHPT_POLYGON, 1,
                             [[(0, 0), (0, 40), (40, 40), (40, 0), (0, 0)],
                              [(10, 10), (20, 10), (20, 20), (10, 20),(10, 10)],
                              ])
    test_shpobject(obj)
    outfile.write_object(-1, obj)

    # close the file.
    outfile.close()

def read_shapefile(filename):
    print "\n* Reading a ShapeFile"
    
    # open the shapefile
    shp = shapelib.ShapeFile(filename)

    # the info method returns a tuple (num_shapes, type, min, max) where
    # num_shapes is the number of shapes, type is the type code (one of
    # the SHPT* constants defined in the shapelib module) and min and
    # max are 4-element lists with the min. and max. values of the
    # vertices.
    print "info:", shp.info()

    # the cobject method returns a PyCObject containing the shapelib
    # SHPHandle. This is useful for passing shapefile objects to
    # C-Python extensions.
    print "cobject:", shp.cobject()
    
    n = shp.info()[0]
    for i in range(n):
        obj = shp.read_object(i)
        print "\nread_object(%i):" % i
        test_shpobject(obj)

    print "\n* SHPTree:"
    
    # build a quad tree from the shapefile. The first argument must be
    # the return value of the shape file object's cobject method (this
    # is currently needed to access the shape file at the C-level). The
    # second argument is the dimension and the third the maximum depth.
    # 0 means to guess an appropriate depth
    tree = shptree.SHPTree(shp.cobject(), 2, 0)

    # Retrieve the ids for a region. Here we just use the extents of the
    # object previously read from the shapefile
    minima, maxima = obj.extents()
    print tree.find_shapes(minima[:2], maxima[:2])


print "--- testing shapelib ---"

make_shapefile(filename)
read_shapefile(filename)

#
#		Test MultiPatch shapefiles
#

def make_multipatch(filename):
    print "\n* Creating multipatch ShapeFile"
    
    # Create a shapefile with multipatches
    outfile = shapelib.create(filename, shapelib.SHPT_MULTIPATCH)

    # Create a quad as a triangle strip and as a triangle fan, in ONE object!
    # Multipatch shapefiles use XYZM vertices, but you can get away with
    # only specifying X and Y, Z and M are zero by default.
    print "\nA triangle strip"
    obj = shapelib.SHPObject(shapelib.SHPT_MULTIPATCH, 0,
        [[(0, 0), (0, 10), (10, 0), (10, 10)],
         [(20, 20), (20, 30), (30, 30), (30, 20)]], 
        [shapelib.SHPP_TRISTRIP, shapelib.SHPP_TRIFAN])
    test_shpobject(obj)
    outfile.write_object(-1, obj)
    
    # A polygon as an Outer ring and inner ring, with XYZ coordinates
    # and measure values M.  Here we will use the part types to specify 
    # their particular type.
    #
    # You can have more than one polygon in a single Object, as long
    # as you obey the following sequence: each polygon starts with an
    # outer ring, followed by its holes as inner rings.  
    #
    # None is also accepted as M value to specify no-data.  The ESRI
    # Shapefile specs define any M value smaller than 1e-38 as no-data.
    # shapelib will store no-data as a zero. 
    #
    # If you don't need the M value, you can leave it out and use triples 
    # as vertices instead.  For the first half of the inner ring,
    # we used None to specify no-data.  In the second half, we just 
    # omitted it.
    #
    print "\nA polygon as outer ring and inner ring with XYZM coordinates"
    obj = shapelib.SHPObject(shapelib.SHPT_MULTIPATCH, 1,
        [[(0, 0, 0, 35.3), (0, 40, 10, 15.4), (40, 40, 20, 9.5), (40, 0, 10, 24.6), (0, 0, 0, 31.8)],
         [(10, 10, 5, None), (20, 10, 10, None), (20, 20, 15), (10, 20, 10, 20),(10, 10, 5)]],
        [shapelib.SHPP_OUTERRING, shapelib.SHPP_INNERRING])
    test_shpobject(obj)
    outfile.write_object(-1, obj)

    # close the file.
    outfile.close()
    

print "--- testing multipatch ---"

make_multipatch("multipatch")
read_shapefile("multipatch")
    
#
#       Test the DBF file module.
#

print "\n\n--- testing dbflib ---"

def make_dbf(file):
    # create a new dbf file and add three fields.
    dbf = dbflib.create(file)
    dbf.add_field("NAME", dbflib.FTString, 20, 0)
    dbf.add_field("INT", dbflib.FTInteger, 10, 0)
    dbf.add_field("FLOAT", dbflib.FTDouble, 10, 4)
    dbf.add_field("BOOL", dbflib.FTLogical, 1, 0)

def add_dbf_records(file):
    # add some records to file
    dbf = dbflib.open(file, "r+b")
    # Records can be added as a dictionary...
    dbf.write_record(0, {'NAME': "Weatherwax", "INT":1, "FLOAT":3.1415926535, "BOOL":True})
    # ... or as a sequence
    dbf.write_record(1, ("Ogg", 2, -1000.1234, False))

def list_dbf(file):
    # print the contents of a dbf file to stdout
    dbf = dbflib.DBFFile(file)
    print "%d records, %d fields" % (dbf.record_count(), dbf.field_count())
    format = ""
    for i in range(dbf.field_count()):
        type, name, len, decc = dbf.field_info(i)
        if type == dbflib.FTString:
            format = format + " %%(%s)%ds" % (name, len)
        elif type == dbflib.FTInteger:
            format = format + " %%(%s)%dd" % (name, len)
        elif type == dbflib.FTDouble:
            format = format + " %%(%s)%dg" % (name, len)
        elif type == dbflib.FTLogical:
            format = format + " %%(%s)s" % name
    print format
    for i in range(dbf.record_count()):
        print format % dbf.read_record(i)


make_dbf(filename)
add_dbf_records(filename)
list_dbf(filename)
