# Copyright (C) 2003 by Intevation GmbH
# Authors:
# Bernhard Herzog <bh@intevation.de>
#
# This program is free software under the LGPL (>=v2)
# Read the file COPYING coming with the software for details.

"""Test cases for the dbflib python bindings"""

__version__ = "$Revision$"
# $Source$
# $Id$

import unittest
import os
import shutil

class TestDBF(unittest.TestCase):

	def setUp(self):
		self.testdir = "testdbf-dir"
		if os.path.exists(self.testdir):
			shutil.rmtree(self.testdir)
		os.mkdir(self.testdir)
		self.testpath = os.path.join(self.testdir, "test")
		
		import dbflib
		self.fields = [
			("NAME", dbflib.FTString, 20, 0),
			("INT", dbflib.FTInteger, 10, 0),
			("FLOAT", dbflib.FTDouble, 10, 4),
			("BOOL", dbflib.FTLogical, 1, 0)
			]
		self.records = [
			('Weatherwax', 1, 3.1415926535, True),
			('Ogg', 2, -1000.1234, False),
			('x\u03C0\u03C1\u03C2', 10, 0, 1),
			]

	def test_add_field(self):
		'''Test whethe add_field reports exceptions'''
		import dbflib
		dbf = dbflib.create(self.testpath)
		# For strings the precision parameter must be 0
		#self.assertRaises(RuntimeError, dbf.add_field, "str", dbflib.FTString, 10, 5)

	def test_dbflib_flow(self):
		self.__make_dbf()
		self.__read_dbf()
		
	def __make_dbf(self):
		import dbflib
		# create a new dbf file and add three fields.
		dbf = dbflib.create(self.testpath, code_page=dbflib.CPG_UTF_8, return_unicode=True)
		for name, type, width, decimals in self.fields:
			dbf.add_field(name, type, width, decimals)

		# Records can be added as a dictionary...
		keys = [field[0] for field in self.fields]
		dbf.write_record(0, dict(zip(keys, self.records[0])))
		
		# ... or as a sequence
		dbf.write_record(1, self.records[1])
		
		# ... or as individual attributes
		for i, value in enumerate(self.records[2]):
			dbf.write_attribute(2, i, value)
		
		dbf.close()

	def __read_dbf(self):
		import dbflib
		dbf = dbflib.DBFFile(self.testpath, return_unicode=True)
		# test the fields
		self.assertEqual(dbf.field_count(), len(self.fields))
		for i in range(dbf.field_count()):
			type, name, width, decimals = dbf.field_info(i)
			self.assertEqual((name, type, width, decimals), self.fields[i])

		# try to read individual attributes (one field within a record)
		self.assertEqual(dbf.record_count(), len(self.records))
		for i in range(dbf.record_count()):
			for k in range(dbf.field_count()):
				self.__assertEqual(dbf.read_attribute(i, k), self.records[i][k])

		# try to read complete records (they are returned as dictionaries)
		keys = [f[0] for f in self.fields]
		for i in range(dbf.record_count()):
			rec = dbf.read_record(i)
			self.assertTrue(isinstance(rec, dict))
			for k, key in enumerate(keys):
				self.__assertEqual(rec[key], self.records[i][k])
		
		# try to read past bounds
		self.assertRaises(IndexError, dbf.read_record, -1)
		self.assertRaises(IndexError, dbf.read_record, dbf.record_count())
		self.assertRaises(IndexError, dbf.read_attribute, 0, -1)
		self.assertRaises(IndexError, dbf.read_attribute, -1, 0)
		self.assertRaises(IndexError, dbf.read_attribute, dbf.record_count(), 0)
		self.assertRaises(IndexError, dbf.read_attribute, 0, dbf.field_count())

	def __assertEqual(self, a, b, msg=None):
		if isinstance(a, float):
			self.assertAlmostEqual(a, b, 4, msg)
		else:
			self.assertEqual(a, b, msg)


if __name__ == "__main__":
    unittest.main()
