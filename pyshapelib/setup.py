
import os
import os.path
import sys
from distutils.core import setup, Extension
from distutils.util import convert_path

def find_shapelib():
	'''
	try to determine the directory where the shapelib source files are.
	There are currently two supported situations.
	
	1. "Standalone" build: the parent directory is the shapelib source
	   directory
	2. Built in the Thuban source tree where ../shapelib/ relative to the
	   directory containing this setup.py contains (the relevant parts of)
	   shapelib
	
	3. Binary build with e.g. bdist_rpm.  This takes place deep in the
	   build directory.
	
	os.path expects filenames in OS-specific form so we have to construct
	the files with os.path functions. distutils, OTOH, uses posix-style
	filenames exclusively, so we use posix conventions when making
	filenames for distutils.
	'''
	for shp_dir in ["..", "../shapelib", "../../../../../../shapelib"]:
		if (os.path.isdir(convert_path(shp_dir))
			and os.path.exists(os.path.join(convert_path(shp_dir), "shpopen.c"))):
			# shp_dir contains shpopen.c, so assume it's the directory with
			# the shapefile library to use
			return shp_dir
	print >>sys.stderr, "no shapelib directory found"
	sys.exit(1)

shp_dir = find_shapelib()



def find_sahooks_files():
	''' 
	Return a filelist of additional files implementing the SA hooks.
	'''
	path = shp_dir + "/safileio.c"
	if not os.path.exists(path):
		return []
	return [path]

sahooks_files = find_sahooks_files()



def determine_macros():
	'''
	Return the macros to define when compiling the dbflib wrapper.

	The returned list specifies following macros:
	- HAVE_UPDATE_HEADER, which is 
	'1' if the dbflib version we will be compiling with has the
	DBFUpdateHeader function and '0' otherwise.  To check whether
	DBFUpdateHeader is available, we scan shapefil.h for the string
	'DBFUpdateHeader'.
	- HAVE_CODE_PAGE, which is '1' if the dbflib version we will 
	compiling with has the DBFGetCodePage function and '0' otherwise.
	Again, shapefil.h is scanned to check this.
	'''
	f = open(convert_path(shp_dir + "/shapefil.h"))
	contents = f.read()
	f.close()
	
	def have(keyword):
		if keyword in contents:
			return "1"
		return "0"
	
	return [
		("HAVE_UPDATE_HEADER", have("DBFUpdateHeader")),
		("HAVE_CODE_PAGE", have("DBFGetCodePage")),
		("HAVE_DELETE_FIELD", have("DBFDeleteField")),
		("DISABLE_CVSID", "1")]

macros = determine_macros()



def determine_cflags():
	if "win32" in sys.platform:
		# assume we're going to use MSVC.  
		return [
			"/wd4996" # disable warning on "potential unsafe" strncpy
			]
	return []


cflags = determine_cflags()



def make_extension(name, *sources):
	return Extension(name, list(sources), include_dirs=[shp_dir], define_macros=macros, extra_compile_args=cflags)




extensions = [
	make_extension("shapelib", "shapelibmodule.c", shp_dir + "/shpopen.c", shp_dir + "/shptree.c", *sahooks_files),
	make_extension("shptree", "shptreemodule.c"),
	make_extension("dbflib", "dbflibmodule.c", shp_dir + "/dbfopen.c", *sahooks_files)
	]



setup(name = "pyshapelib",
	version = "1.1",
	description = "Python bindings for shapelib",
	author = "Bernhard Herzog, Bram de Greve",
	author_email = "bh@intevation.de, bram.degreve@bramz.net",
	url = "http://wald.intevation.org/frs/?group_id=48",
	ext_modules = extensions)

