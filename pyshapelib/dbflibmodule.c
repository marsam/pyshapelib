/* Copyright (c) 2001-2008 by Intevation GmbH
 * Authors:
 * Bram de Greve <bram.degreve@bramz.net>
 * Bernhard Herzog <bh@intevation.de>
 *
 * This program is free software under the LGPL (>=v2)
 * Read the file COPYING coming with pyshapelib for details.
 */

#include "pyshapelib_common.h"

#define DEFAULT_CODEC "cp1252"
#define DEFAULT_CODE_PAGE "LDID/87" // 0x57

static PyObject* default_codecs_map = NULL;

/* --- DBFFile ------------------------------------------------------------------------------------------------------- */

typedef struct {
	PyObject_HEAD
	DBFHandle handle;
	char* codec;
	int return_unicode;
} DBFFileObject;



/* allocator 
*/
static PyObject* dbffile_new(PyTypeObject* type, PyObject* args, PyObject* kwds)
{
	DBFFileObject* self;	
	self = (DBFFileObject*) type->tp_alloc(type, 0);
	self->handle = NULL;
	self->codec = NULL;
	self->return_unicode = 1;
	return (PyObject*) self;
}



/* deallocator
*/
static void dbffile_dealloc(DBFFileObject* self)
{
	DBFClose(self->handle);
	self->handle = NULL;
	PyMem_Free(self->codec);
	self->codec = NULL;
	Py_TYPE(self)->tp_free((PyObject*)self);
}


static int dbffile_init_codec(DBFFileObject* self, PyObject* codecs_map)
{
	size_t n = 0;
	char* codec = DEFAULT_CODEC;
#if HAVE_CODE_PAGE
	PyObject* ocodec = NULL;
	char* code_page = (char*) DBFGetCodePage(self->handle);

	PyMem_Free(self->codec);
	self->codec = NULL;
	if (codecs_map && codecs_map != Py_None)
	{
		if (!PyMapping_Check(codecs_map))
		{
			PyErr_SetString(PyExc_TypeError, "codecs_map is not mapable");
			return -1;
		}
	}
	else
	{
		codecs_map = default_codecs_map;
	}	

	if (code_page)
	{
		ocodec = PyMapping_GetItemString(codecs_map, code_page);
		if (!ocodec)
		{
			PyErr_Format(PyExc_KeyError, "code_page '%s' not found in codecs_map", code_page);
			return -1;
		}
		if (PyUnicode_Check(ocodec))
		{
			ocodec = PyUnicode_AsUTF8String(ocodec); // is this accepted?  UTF8 codec names?
		}
		else
		{
			Py_INCREF(ocodec);
		}
		codec = PYSHAPELIB_ASSTRING(ocodec);
		Py_DECREF(ocodec);
		if (!codec)
		{
			return -1;
		}
	}
#endif
	
	n = strlen(codec);
	self->codec = PyMem_Malloc(n + 1);
	if (!self->codec)
	{
		PyErr_NoMemory();
		return -1;
	}
	memcpy(self->codec, codec, n + 1);

	return 0;
}


/* constructor
*/
static int dbffile_init(DBFFileObject* self, PyObject* args, PyObject* kwds)
{
	SAHooks hooks;
	char* file = NULL;
	char* mode = "rb";
	PyObject* return_unicode = NULL;
	PyObject* codecs_map = NULL;
#if HAVE_CODE_PAGE
	static char *kwlist[] = {"name", "mode", "return_unicode", "codecs_map", NULL};
#else
	static char *kwlist[] = {"name", "mode", "return_unicode", NULL};
#endif

	DBFClose(self->handle);
	self->handle = NULL;

#if HAVE_CODE_PAGE
	if (!PyArg_ParseTupleAndKeywords(args, kwds, "et|sOO:DBFFile", kwlist, 
		PYSHAPELIB_FILENAME_ENCODING, &file, &mode, &return_unicode, &codecs_map)) return -1;	
#else
	if (!PyArg_ParseTupleAndKeywords(args, kwds, "et|sO:DBFFile", kwlist, 
		PYSHAPELIB_FILENAME_ENCODING, &file, &mode, &return_unicode)) return -1;	
#endif

	PYSHAPELIB_SETUPHOOKS(&hooks);
	self->handle = DBFOpenLL(file, mode, &hooks);

	if (!self->handle)
	{
		PyErr_SetFromErrnoWithFilename(PyExc_IOError, file);
		PyMem_Free(file);
		return -1;
	}
	PyMem_Free(file);

	self->return_unicode = return_unicode ? PyObject_IsTrue(return_unicode) : 1;
	if (self->return_unicode < 0)
	{
		DBFClose(self->handle);
		self->handle = NULL;
		return -1;
	}

	if (dbffile_init_codec(self, codecs_map) != 0)
	{
		DBFClose(self->handle);
		self->handle = NULL;
		return -1;
	}

	return 0;
}



static PyObject* dbffile_close(DBFFileObject* self)
{
	DBFClose(self->handle);
	self->handle = NULL;
	PyMem_Free(self->codec);
	self->codec = NULL;
	Py_RETURN_NONE;
}



/** decode to unicode object 
 */
static PyObject* dbffile_decode_string(DBFFileObject* self, const char* string)
{
	if (self->return_unicode)
	{
		return PyUnicode_Decode(string, strlen(string), self->codec, NULL);
	}
#if PYSHAPELIB_IS_PY3K
	return PyBytes_FromString(string);
#else
	return PyString_FromString(string);
#endif
}

/** encode unicode object to normal Python string/bytes object 
 */
static PyObject* dbffile_encode_string(DBFFileObject* self, PyObject* string)
{
#if PYSHAPELIB_IS_PY3K
	if (PyBytes_Check(string))
#else
	if (PyString_Check(string))
#endif
	{
		Py_INCREF(string);
		return string;
	}
	if (PyUnicode_Check(string))
	{
		return PyUnicode_AsEncodedString(string, self->codec, NULL);
	}

	PyErr_SetString(PyExc_TypeError, "value is neither a string or unicode object");
	return NULL;
}



static PyObject* dbffile_field_count(DBFFileObject* self)
{
	return PYSHAPELIB_FROMLONG((long)DBFGetFieldCount(self->handle));
}



static PyObject* dbffile_record_count(DBFFileObject* self)
{
	return PYSHAPELIB_FROMLONG((long)DBFGetRecordCount(self->handle));
}



static int check_field_index(DBFFileObject* self, int field)
{
	if (field < 0 || field >= DBFGetFieldCount(self->handle))
	{
		PyErr_Format(PyExc_IndexError,
				"field index %d out of bounds (field count: %d)",
				field, DBFGetFieldCount(self->handle));
		return -1;
	}
	return field;
}


static int check_record_index(DBFFileObject* self, int record)
{
	if (record < 0 || record >= DBFGetRecordCount(self->handle))
	{
		PyErr_Format(PyExc_IndexError,
				"record index %d out of bounds (record count: %d)",
				record, DBFGetRecordCount(self->handle));
		return -1;
	}
	return record;
}



static PyObject* dbffile_field_info(DBFFileObject* self, PyObject* args)
{
	char field_name[12];
	int field, width = 0, decimals = 0, field_type;
	PyObject* name_object = NULL;
	
	if (!PyArg_ParseTuple(args, "i:field_info", &field)) return NULL;
	if (check_field_index(self, field) < 0) return NULL;
	
	field_name[0] = '\0';
	field_type = DBFGetFieldInfo(self->handle, field, field_name, &width, &decimals);
	if (field_type == FTDouble && decimals == 0)
	{
		field_type = FTInteger;
	}
	name_object = dbffile_decode_string(self, field_name);
	
	return Py_BuildValue("iOii", field_type, name_object, width, decimals);
}



static PyObject* dbffile_add_field(DBFFileObject* self, PyObject* args)
{
	PyObject *oname = NULL, *name = NULL;
	int type, width, decimals;
	int field;
	
	if (!PyArg_ParseTuple(args, "Uiii:add_field", &oname, &type, &width, &decimals))
	{
		PyErr_Clear();
		if (!PyArg_ParseTuple(args, "Siii:add_field", &oname, &type, &width, &decimals)) return NULL;
	}
	
	name = dbffile_encode_string(self, oname);
	if (!name) return NULL;

	field = DBFAddField(self->handle, PYSHAPELIB_ASSTRING(name), (DBFFieldType)type, width, decimals);
	Py_DECREF(name);
	
	if (field < 0)
	{
		PyErr_SetString(PyExc_ValueError, "Failed to add field");
		return NULL;
	}
	return PYSHAPELIB_FROMLONG((long)field);
}



/* The method relies on the DBFDeleteField method which is not
* available in shapelib <= 1.3.0.  setup.py defines
* HAVE_DELETE_FIELD's value depending on whether the function is
* available in the shapelib version the code is compiled with.
*/
#if HAVE_DELETE_FIELD
static PyObject* dbffile_delete_field(DBFFileObject* self, PyObject* args)
{
	int field;
	if (!PyArg_ParseTuple(args, "i:field_info", &field)) return NULL;
	if (check_field_index(self, field) < 0) return NULL;
	
	if (!DBFDeleteField(self->handle, field))
	{
		PyErr_SetString(PyExc_RuntimeError, "failed to delete field.");
	}

	Py_RETURN_NONE;
}
#endif



/* Read one attribute from the dbf handle and return it as a new python object
*
* If an error occurs, set the appropriate Python exception and return
* NULL.
*
* Assume that the values of the record and field arguments are valid.
* The name argument will be passed to DBFGetFieldInfo as is and should
* thus be either NULL or a pointer to an array of at least 12 chars
*/
static PyObject* do_read_attribute(DBFFileObject* self, int record, int field, char * name)
{
	int type, decimals;
	const char* string;
	type = DBFGetFieldInfo(self->handle, field, name, NULL, &decimals);
	
	/* For strings NULL and the empty string are indistinguishable
	* in DBF files. We prefer empty strings instead for backwards
	* compatibility reasons because older wrapper versions returned
	* emtpy strings as empty strings.
	*/
	if (type != FTString && DBFIsAttributeNULL(self->handle, record, field))
	{
		Py_RETURN_NONE;
	}
	else
	{
		switch (type)
		{
		case FTString:
			string = DBFReadStringAttribute(self->handle, record, field);
			if (string) return dbffile_decode_string(self, string);

		case FTInteger:
			return PyLong_FromDouble(DBFReadDoubleAttribute(self->handle, record, field));

		case FTDouble:
			if (decimals == 0)
			{
				return PyLong_FromDouble(DBFReadDoubleAttribute(self->handle, record, field));
			}
			return PyFloat_FromDouble(DBFReadDoubleAttribute(self->handle, record, field));
			
		case FTLogical:
			string = DBFReadLogicalAttribute(self->handle, record, field);
			if (string)
			{
				switch (string[0])
				{
				case '?':
					Py_RETURN_NONE;
				case 'F':
				case 'f':
				case 'N':
				case 'n':
					Py_RETURN_FALSE;
				case 'T':
				case 't':
				case 'Y':
				case 'y':
					Py_RETURN_TRUE;
				}
			}
			break;

		default:
			PyErr_Format(PyExc_TypeError, "Invalid field data type %d", type);
			return NULL;
		}
	}
	
	PyErr_Format(PyExc_IOError, "Can't read value for row %d column %d", record, field);
	return NULL;
}



/* the read_attribute method. Return the value of the given record and
* field as a python object of the appropriate type.
*/
static PyObject* dbffile_read_attribute(DBFFileObject* self, PyObject* args)
{
	int record, field;

	if (!PyArg_ParseTuple(args, "ii:read_attribute", &record, &field)) return NULL;
	if (check_record_index(self, record) < 0) return NULL;
	if (check_field_index(self, field) < 0) return NULL;

	return do_read_attribute(self, record, field, NULL);
}



/* the read_record method. Return the record record as a dictionary with
* whose keys are the names of the fields, and their values as the
* appropriate Python type.
*/
static PyObject* dbffile_read_record(DBFFileObject* self, PyObject* args)
{
	int record;
	int num_fields;
	int i;
	char name[12];
	PyObject *dict;
	PyObject *value = NULL;

	if (!PyArg_ParseTuple(args, "i:read_record", &record)) return NULL;
	if (check_record_index(self, record) < 0) return NULL;

	dict = PyDict_New();
	if (!dict) return NULL;
	
	num_fields = DBFGetFieldCount(self->handle);
	for (i = 0; i < num_fields; i++)
	{
		value = do_read_attribute(self, record, i, name);
		if (!value || PyDict_SetItemString(dict, name, value) < 0) goto fail;
		Py_DECREF(value);
		value = NULL;
	}

	return dict;

fail:
	Py_XDECREF(value);
	Py_DECREF(dict);
	return NULL;
}



/* write a single field of a record. */
static int do_write_attribute(DBFFileObject* self, int record, int field, int type, PyObject* value)
{
	PyObject* string_value = NULL;
	int int_value;
	double double_value;
	int logical_value;

	if (value == Py_None)
	{
		if (DBFWriteNULLAttribute(self->handle, record, field)) return 1;
	}
	else
	{
		switch (type)
		{
		case FTString:
			string_value = dbffile_encode_string(self, value);
			if (!string_value) return 0;
			if (DBFWriteStringAttribute(self->handle, record, field, PYSHAPELIB_ASSTRING(string_value)))
			{
				Py_DECREF(string_value);
				return 1;
			}
			Py_DECREF(string_value);
			break;

		case FTInteger:
			int_value = PYSHAPELIB_ASLONG(value);
			if (int_value == -1 && PyErr_Occurred()) return 0;
			if (DBFWriteIntegerAttribute(self->handle, record, field, int_value)) return 1;
			break;

		case FTDouble:
			double_value = PyFloat_AsDouble(value);
			if (double_value == -1 && PyErr_Occurred()) return 0;
			if (DBFWriteDoubleAttribute(self->handle, record, field, double_value)) return 1;
			break;
			
		case FTLogical:
			logical_value = PyObject_IsTrue(value);
			if (logical_value == -1) return 0;
			if (DBFWriteLogicalAttribute(self->handle, record, field, logical_value ? 'T' : 'F')) return 1;
			break;

		default:
			PyErr_Format(PyExc_TypeError, "Invalid field data type %d", type);
			return 0;
		}
	}

	PyErr_Format(PyExc_IOError,	"can't write field %d of record %d", field, record);
	return 0;
}



static PyObject* dbffile_write_attribute(DBFFileObject* self, PyObject* args)
{
	int record, field;
	PyObject* value;
	int type;

	if (!PyArg_ParseTuple(args, "iiO:write_attribute", &record, &field, &value)) return NULL;
	if (check_field_index(self, field) < 0) return NULL;
	
	type = DBFGetFieldInfo(self->handle, field, NULL, NULL, NULL);
	if (!do_write_attribute(self, record, field, type, value)) return NULL;
	Py_RETURN_NONE;
}



static PyObject* dbffile_write_record(DBFFileObject* self, PyObject* args)
{
	int record;
	PyObject* record_object;
	int i, num_fields;
	
	int type;
	char name[12];
	PyObject* value = NULL;
	
	if (!PyArg_ParseTuple(args, "iO:write_record", &record, &record_object)) return NULL;
	
	num_fields = DBFGetFieldCount(self->handle);
	
	/* mimic ShapeFile functionality where id = -1 means appending */
	if (record == -1)
	{
		record = num_fields;
	}

	if (PySequence_Check(record_object))
	{
		/* It's a sequence object. Iterate through all items in the
		* sequence and write them to the appropriate field.
		*/
		if (PySequence_Length(record_object) != num_fields)
		{
			PyErr_SetString(PyExc_TypeError, "record must have one item for each field");
			return NULL;
		}
		for (i = 0; i < num_fields; ++i)
		{
			type = DBFGetFieldInfo(self->handle, i, NULL, NULL, NULL); 
			value = PySequence_GetItem(record_object, i);
			if (!value) return NULL;
			if (!do_write_attribute(self, record, i, type, value)) 
			{
				Py_DECREF(value);
				return NULL;
			}
			Py_DECREF(value);
		}
	}
	else
	{
		/* It's a dictionary-like object. Iterate over the names of the
		* known fields and write the corresponding item
		*/
		for (i = 0; i < num_fields; ++i)
		{
			name[0] = '\0';
			type = DBFGetFieldInfo(self->handle, i, name, NULL, NULL);
			value = PyDict_GetItemString(record_object, name);
			if (value && !do_write_attribute(self, record, i, type, value)) return NULL;
		}
	}
	
	return PYSHAPELIB_FROMLONG((long)record);
}



static PyObject* dbffile_repr(DBFFileObject* self)
{
	/* TODO: it would be nice to do something like "dbflib.DBFFile(filename, mode)" instead */
	return PYSHAPELIB_FROMFORMAT("<dbflib.DBFFile object at %p>", self->handle);
}



/* The commit method implementation
*
* The method relies on the DBFUpdateHeader method which is not
* available in shapelib <= 1.2.10.  setup.py defines
* HAVE_UPDATE_HEADER's value depending on whether the function is
* available in the shapelib version the code is compiled with.
*/
#if HAVE_UPDATE_HEADER
static PyObject* dbffile_commit(DBFFileObject* self)
{
	DBFUpdateHeader(self->handle);
	Py_RETURN_NONE;
}
#endif


#if HAVE_CODE_PAGE

static PyObject* dbffile_code_page(DBFFileObject* self, void* closure)
{
	const char* code_page = DBFGetCodePage(self->handle);
	if (!code_page)
	{
		Py_RETURN_NONE;
	}
	return PYSHAPELIB_FROMSTRING(code_page);
}

#endif

static PyObject* dbffile_codec(DBFFileObject* self, void* closure)
{
	if (!self->codec)
	{
		Py_RETURN_NONE;
	}
	return PYSHAPELIB_FROMSTRING(self->codec);
}


static struct PyMethodDef dbffile_methods[] = 
{
	{"close", (PyCFunction)dbffile_close, METH_NOARGS, 
		"close() -> None\n\n"
		"closes DBFFile"},
	{"field_count", (PyCFunction)dbffile_field_count, METH_NOARGS, 
		"field_count() -> integer\n\n"
		"returns number of fields currently defined"},
	{"record_count", (PyCFunction)dbffile_record_count, METH_NOARGS, 
		"record_count() -> integer\n\n"
		"returns number of records that currently exist"},
	{"field_info", (PyCFunction)dbffile_field_info, METH_VARARGS,
		"field_info(field_index) -> (type, name, width, decimals)\n\n"
		"returns info of a field as a tuple with:\n"
		"- type: the type of the field corresponding to the integer value of one "
		" of the constants FTString, FTInteger, ...\n"
		"- name: the name of the field as a string\n"
		"- width: the width of the field as a number of characters\n"
		"- decimals: the number of decimal digits" },
	{"add_field", (PyCFunction)dbffile_add_field, METH_VARARGS,
		"add_field(type, name, width, decimals) -> field_index\n\n"
		"adds a new field and returns field index if successful\n"
		"- type: the type of the field corresponding to the integer value of one "
		" of the constants FTString, FTInteger, ...\n"
		"- name: the name of the field as a string\n"
		"- width: the width of the field as a number of characters\n"
		"- decimals: the number of decimal digits" },
#if HAVE_DELETE_FIELD
	{"delete_field", (PyCFunction)dbffile_delete_field, METH_NOARGS, 
		"delete_field(field_index) -> None\n\n"
		"removes an entire column"},
#endif
	{"read_attribute", (PyCFunction)dbffile_read_attribute, METH_VARARGS, 
		"read_attribute(record_index, field_index) -> value\n\n"
		"returns the value of one field of a record"},
	{"read_record", (PyCFunction)dbffile_read_record, METH_VARARGS, 
		"read_record(record_index) -> dict\n\n"
		"returns an entire record as a dictionary of field names and values"},
	{"write_attribute", (PyCFunction)dbffile_write_attribute, METH_VARARGS, 
		"write_attribute(record_index, field_index, new_value)\n"
		"writes a single field of a record"},
	{"write_record", (PyCFunction)dbffile_write_record, METH_VARARGS, 
		"write_record(record_index, record) -> record_index\n\n"
		"Writes an entire record as a dict or a sequence, and return index of record\n"
		"Record can either be a dictionary in which case the keys are used as field names, "
		"or a sequence that must have an item for every field (length = field_count())"},
#if HAVE_UPDATE_HEADER
	{"commit", (PyCFunction)dbffile_commit, METH_NOARGS, 
		"commit() -> None"},
#endif
	{NULL}
};



static struct PyGetSetDef dbffile_getsetters[] = 
{
	{"codec", (getter)dbffile_codec, NULL, "Python codec name used to encode or decode Unicode strings (read-only)" },
#if HAVE_CODE_PAGE
	{"code_page", (getter)dbffile_code_page, NULL, "DBF Code Page from LDID or .CPG file (read-only)" },
#endif
	{NULL}
};



static PyTypeObject DBFFileType = PYSHAPELIB_DEFINE_TYPE(DBFFileObject, dbffile, "shapelib.DBFFile", 0);



/* --- dbflib -------------------------------------------------------------------------------------------------------- */

static PyObject* dbflib_open(PyObject* module, PyObject* args, PyObject* kwds)
{
	return PyObject_Call((PyObject*)&DBFFileType, args, kwds);
}



static PyObject* dbflib_create(PyObject* module, PyObject* args, PyObject* kwds)
{
	SAHooks hooks;
	char* file;
	DBFFileObject* result;
	DBFHandle handle = NULL;
	PyObject* return_unicode = NULL;
	PyObject* codecs_map = NULL;
	char* code_page = DEFAULT_CODE_PAGE;

#if HAVE_CODE_PAGE
	static char *kwlist[] = {"name", "code_page", "return_unicode", "codecs_map", NULL};
#else
	static char *kwlist[] = {"name", "return_unicode", NULL};
#endif

	PYSHAPELIB_SETUPHOOKS(&hooks);

#if HAVE_CODE_PAGE
	if (!PyArg_ParseTupleAndKeywords(args, kwds, "et|sOO:create", kwlist, PYSHAPELIB_FILENAME_ENCODING, 
			&file, &code_page, &return_unicode, &codecs_map)) return NULL;
	handle = DBFCreateLL(file, code_page, &hooks);
#else
	if (!PyArg_ParseTupleAndKeywords(args, kwds, "et|O:create", kwlist, PYSHAPELIB_FILENAME_ENCODING, 
			&file, &return_unicode)) return NULL;
	handle = DBFCreateLL(file, &hooks);
#endif

	if (!handle)
	{
		PyErr_SetFromErrnoWithFilename(PyExc_IOError, file);
		PyMem_Free(file);
		return NULL;
	}
	PyMem_Free(file);

	result = PyObject_New(DBFFileObject, &DBFFileType);
	if (!result)
	{
		DBFClose(handle);
		return PyErr_NoMemory();
	}
	
	result->handle = handle;
	result->return_unicode = return_unicode ? PyObject_IsTrue(return_unicode) : 1;
	result->codec = NULL;
	
	if (result->return_unicode < 0)
	{
		dbffile_dealloc(result);
		return NULL;
	}

	if (dbffile_init_codec(result, codecs_map) != 0)
	{
		dbffile_dealloc(result);
		return NULL;
	}

	return (PyObject*) result;
}



static struct PyMethodDef dbflib_methods[] = 
{
	{"open", (PyCFunction)dbflib_open, METH_VARARGS | METH_KEYWORDS,
#if HAVE_CODE_PAGE
		"open(name, mode='rb', return_unicode=True, codecs_map=None) -> DBFFile\n\n"
#else
		"open(name, mode='rb', return_unicode=True) -> DBFFile\n\n"
#endif
		"opens a DBFFile" },
	{"create", (PyCFunction)dbflib_create, METH_VARARGS | METH_KEYWORDS, 
#if HAVE_CODE_PAGE
		"create(name, code_page='" DEFAULT_CODE_PAGE "', return_unicode=True, codecs_map=None) -> DBFFile\n\n"
#else
		"create(name, return_unicode=True) -> DBFFile\n\n"
#endif
		"create a DBFFile " },
	{NULL}
};


#if HAVE_CODE_PAGE

void add_ldid(PyObject* module, int ldid, const char* codec, const char* name)
{
	char code_page[64];
	char constant[64];
	PyObject* ocodec = PYSHAPELIB_FROMSTRING(codec);
	sprintf(code_page, "LDID/%i", ldid);
	PyDict_SetItemString(default_codecs_map, code_page, ocodec);
	Py_XDECREF(ocodec);
	sprintf(constant, "LDID_%s", name);
	PyModule_AddStringConstant(module, constant, code_page);
}

void add_cpg(PyObject* module, char* code_page, const char* codec, const char* name)
{
	char constant[64];
	PyObject* ocodec = PYSHAPELIB_FROMSTRING(codec);
	PyDict_SetItemString(default_codecs_map, code_page, PYSHAPELIB_FROMSTRING(codec));
	Py_XDECREF(ocodec);
	sprintf(constant, "CPG_%s", name);
	PyModule_AddStringConstant(module, constant, code_page);
}

#endif


#if PYSHAPELIB_IS_PY3K

static struct PyModuleDef dbflib_moduledef = {
		PyModuleDef_HEAD_INIT,
		"dbflib",
		NULL,
		0,
		dbflib_methods,
		NULL,
		NULL,
		NULL,
		NULL
};

PyMODINIT_FUNC PyInit_dbflib(void)
#else
PyMODINIT_FUNC initdbflib(void)
#endif
{
#if PYSHAPELIB_IS_PY3K
	PyObject *module = PyModule_Create(&dbflib_moduledef);
#else
	PyObject* module = Py_InitModule("dbflib", dbflib_methods);
#endif
	if (!module) PYSHAPELIB_INITRETURN(NULL);
	
	PYSHAPELIB_ADD_TYPE(DBFFileType, "DBFFile");
	
	PYSHAPELIB_ADD_CONSTANT(FTString);
	PYSHAPELIB_ADD_CONSTANT(FTInteger);
	PYSHAPELIB_ADD_CONSTANT(FTDouble);
	PYSHAPELIB_ADD_CONSTANT(FTLogical);
	PYSHAPELIB_ADD_CONSTANT(FTInvalid);
	PyModule_AddIntConstant(module, "_have_commit", HAVE_UPDATE_HEADER);
	PyModule_AddIntConstant(module, "_have_code_page", HAVE_CODE_PAGE);
	PyModule_AddIntConstant(module, "_have_utf8_hooks", HAVE_UTF8_HOOKS);

#if HAVE_CODE_PAGE
	default_codecs_map = PyDict_New();

	/* table compiled from these resources:
	 * http://www.clicketyclick.dk/databases/xbase/format/dbf.html
	 * http://www.esrinl.com/content/file.asp?id=307
	 * http://msdn2.microsoft.com/en-us/library/aa975345(VS.71).aspx
	 */
	add_ldid(module, 0x00, "cp1252", "NOT_SET");
	add_ldid(module, 0x01, "cp437", "DOS_USA");
	add_ldid(module, 0x02, "cp850", "DOS_INTERNATIONAL");
	add_ldid(module, 0x03, "cp1252", "WINDOWS_ANSI");
	add_ldid(module, 0x04, "mac_roman", "STANDARD_MACINTOSH");
	add_ldid(module, 0x08, "cp865", "DANISH_OEM");
	add_ldid(module, 0x09, "cp437", "DUTCH_OEM");
	add_ldid(module, 0x0a, "cp850", "DUTCH_OEM_2");
	add_ldid(module, 0x0b, "cp437", "FINNISH_OEM");
	add_ldid(module, 0x0d, "cp437", "FRENCH_OEM");
	add_ldid(module, 0x0e, "cp850", "FRENCH_OEM_2");
	add_ldid(module, 0x0f, "cp437", "GERMAN_OEM");
	add_ldid(module, 0x10, "cp850", "GERMAN_OEM_2");
	add_ldid(module, 0x11, "cp437", "ITALIAN_OEM");
	add_ldid(module, 0x12, "cp850", "ITALIAN_OEM_2");
	add_ldid(module, 0x13, "cp932", "JAPANESE_SHIFT_JIS");
	add_ldid(module, 0x14, "cp850", "SPANISH_OEM_2");
	add_ldid(module, 0x15, "cp437", "SWEDISH_OEM");
	add_ldid(module, 0x16, "cp850", "SWEDISH_OEM_2");
	add_ldid(module, 0x17, "cp865", "NORWEGIAN_OEM");
	add_ldid(module, 0x18, "cp437", "SPANISH_OEM");
	add_ldid(module, 0x19, "cp437", "ENGLISH_BRITAIN_OEM");
	add_ldid(module, 0x1a, "cp850", "ENGLISH_BRITAIN_OEM_2");
	add_ldid(module, 0x1b, "cp437", "ENGLISH_US_OEM");
	add_ldid(module, 0x1c, "cp863", "FRENCH_CANADA_OEM");
	add_ldid(module, 0x1d, "cp850", "FRENCH_OEM_2");
	add_ldid(module, 0x1f, "cp852", "CZECH_OEM");
	add_ldid(module, 0x22, "cp852", "HUNGARIAN_OEM");
	add_ldid(module, 0x23, "cp852", "POLISH_OEM");
	add_ldid(module, 0x24, "cp860", "PORTUGUESE_OEM");
	add_ldid(module, 0x25, "cp850", "PORTUGUESE_OEM_2");
	add_ldid(module, 0x26, "cp866", "RUSSIAN_OEM");
	add_ldid(module, 0x37, "cp850", "ENGLISH_US_OEM_2");
	add_ldid(module, 0x40, "cp852", "ROMANIAN_OEM");
	add_ldid(module, 0x4d, "cp936", "CHINESE_GBK_PRC");
	add_ldid(module, 0x4e, "cp949", "KOREAN_ANSI_OEM);");
	add_ldid(module, 0x4f, "cp950", "CHINESE_BIG5_TAIWAN");
	add_ldid(module, 0x50, "cp874", "THAI_ANSI_OEM");
	add_ldid(module, 0x57, "cp1252", "ESRI_ANSI");
	add_ldid(module, 0x58, "cp1252", "WESTERN_EUROPEAN_ANSI");
	add_ldid(module, 0x59, "cp1252", "SPANISH_ANSI");
	add_ldid(module, 0x64, "cp852", "EASTERN_EUROPEAN_MSDOS");
	add_ldid(module, 0x65, "cp866", "RUSSIAN_MSDOS");
	add_ldid(module, 0x66, "cp865", "NORDIC_MSDOS");
	add_ldid(module, 0x67, "cp861", "ICELANDIC_MSDOS");
	add_ldid(module, 0x68, "cp895", "CZECH_MSDOS");
	add_ldid(module, 0x69, "cp620", "POLISH_MSDOS");
	add_ldid(module, 0x6a, "cp737", "GREEK_MSDOS");
	add_ldid(module, 0x6b, "cp857", "TURKISH_MSDOS");
	add_ldid(module, 0x6c, "cp863", "FRENCH_CANADA_MSDOS");
	add_ldid(module, 0x78, "cp950", "TAIWAN_BIG5");
	add_ldid(module, 0x79, "cp949", "HANGUL_WANSUG");
	add_ldid(module, 0x7a, "cp936", "PRC_GBK");
	add_ldid(module, 0x7b, "cp932", "JAPANESE_SHIFT_JIS");
	add_ldid(module, 0x7c, "cp874", "THAI_WINDOWS_MSDOS");
	add_ldid(module, 0x7d, "cp1255", "HEBREW_WINDOWS");
	add_ldid(module, 0x7e, "cp1256", "ARABIC_WINDOWS");
	add_ldid(module, 0x86, "cp737", "GREEK_OEM");
	add_ldid(module, 0x87, "cp852", "SLOVENIAN_OEM");
	add_ldid(module, 0x88, "cp857", "TURKISH_OEM");
	add_ldid(module, 0x96, "mac_cyrillic", "RUSSIAN_MACINTOSH");
	add_ldid(module, 0x97, "mac_latin2", "EASTERN_EUROPEAN_MACINTOSH");
	add_ldid(module, 0x98, "mac_greek", "GREEK_MACINTOSH");
	add_ldid(module, 0xc8, "cp1250", "EASTERN_EUROPEAN_WINDOWS");
	add_ldid(module, 0xc9, "cp1251", "RUSSIAN_WINDOWS");
	add_ldid(module, 0xca, "cp1254", "TURKISH_WINDOWS");
	add_ldid(module, 0xcb, "cp1253", "GREEK_WINDOWS");
	add_ldid(module, 0xcc, "cp1257", "BALTIC_WINDOWS");
	add_cpg(module, "UTF-8", "utf_8", "UTF_8");
	add_cpg(module, "OEM 737", "cp737", "OEM_737");
	add_cpg(module, "OEM 775", "cp755", "OEM_775");
	add_cpg(module, "OEM 852", "cp852", "OEM_852");
	add_cpg(module, "OEM 855", "cp855", "OEM_855");
	add_cpg(module, "OEM 857", "cp857", "OEM_857");
	add_cpg(module, "OEM 860", "cp860", "OEM_860");
	add_cpg(module, "OEM 861", "cp861", "OEM_861");
	add_cpg(module, "OEM 862", "cp862", "OEM_862");
	add_cpg(module, "OEM 863", "cp863", "OEM_863");
	add_cpg(module, "OEM 864", "cp864", "OEM_864");
	add_cpg(module, "OEM 865", "cp865", "OEM_865");
	add_cpg(module, "OEM 866", "cp866", "OEM_866");
	add_cpg(module, "OEM 869", "cp869", "OEM_869");
	add_cpg(module, "OEM 932", "cp932", "OEM_932");
	add_cpg(module, "OEM 950", "cp950", "OEM_950");
	add_cpg(module, "ISO 88591", "iso-8859-1", "ISO_8859_1");
	add_cpg(module, "ISO 88592", "iso-8859-2", "ISO_8859_2");
	add_cpg(module, "ISO 88593", "iso-8859-3", "ISO_8859_3");
	add_cpg(module, "ISO 88594", "iso-8859-4", "ISO_8859_4");
	add_cpg(module, "ISO 88595", "iso-8859-5", "ISO_8859_5");
	add_cpg(module, "ISO 88596", "iso-8859-6", "ISO_8859_6");
	add_cpg(module, "ISO 88597", "iso-8859-7", "ISO_8859_7");
	add_cpg(module, "ISO 88598", "iso-8859-8", "ISO_8859_8");
	add_cpg(module, "ISO 88599", "iso-8859-9", "ISO_8859_9");
	add_cpg(module, "ISO 885910", "iso-8859-10", "ISO_8859_10");
	add_cpg(module, "ISO 885913", "iso-8859-13", "ISO_8859_13");
	add_cpg(module, "ISO 885915", "iso-8859-15", "ISO_8859_15");

#endif

	PYSHAPELIB_INITRETURN(module);
}
