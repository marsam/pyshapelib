/* Copyright (c) 2007-2008 by Intevation GmbH
 * Authors:
 * Bram de Greve <bram.degreve@bramz.net>
 *
 * This program is free software under the LGPL (>=v2)
 * Read the file COPYING coming with pyshapelib for details.
 */

#ifndef PYSHAPELIB_H
#define PYSHAPELIB_H

#include <Python.h>
#include <structmember.h>
#include "shapefil.h"
#include "pyshapelib_api.h"



#ifndef Py_RETURN_NONE
#	define Py_RETURN_NONE return Py_INCREF(Py_None), Py_None
#endif
#ifndef Py_RETURN_TRUE
#	define Py_RETURN_TRUE return Py_INCREF(Py_True), Py_True
#endif
#ifndef Py_RETURN_FALSE
#	define Py_RETURN_FALSE return Py_INCREF(Py_False), Py_False
#endif
#ifndef Py_TYPE
#	define Py_TYPE(ob) (((PyObject*)(ob))->ob_type)
#endif


/* switch to distinguish between 2.x and 3.x Python API where incompatible.
 */
#if PY_MAJOR_VERSION >= 3
#	define PYSHAPELIB_IS_PY3K 1
#endif

#if PYSHAPELIB_IS_PY3K
#	define PYSHAPELIB_ASLONG PyLong_AsLong
#	define PYSHAPELIB_FROMLONG PyLong_FromLong
#	define PYSHAPELIB_ASSTRING PyBytes_AsString
#	define PYSHAPELIB_FROMSTRING PyUnicode_FromString
#	define PYSHAPELIB_FORMAT PyUnicode_Format
#	define PYSHAPELIB_FROMFORMAT PyUnicode_FromFormat
#	define PYSHAPELIB_HEAD_INIT(type, size) PyVarObject_HEAD_INIT(type, size)
#	define PYSHAPELIB_INITRETURN(value) return value
#else
#	define PYSHAPELIB_ASLONG PyInt_AsLong
#	define PYSHAPELIB_FROMLONG PyInt_FromLong
#	define PYSHAPELIB_ASSTRING PyString_AsString
#	define PYSHAPELIB_FROMSTRING PyString_FromString
#	define PYSHAPELIB_FORMAT PyString_Format
#	define PYSHAPELIB_FROMFORMAT PyString_FromFormat
#	define PYSHAPELIB_HEAD_INIT(type, size) PyObject_HEAD_INIT(type) size,
#	define PYSHAPELIB_INITRETURN(value) return
#endif



/* helper to export constants (macros) to Python.
 * The constant in Python will have the same name as in C
 */
#define PYSHAPELIB_ADD_CONSTANT(constant) PyModule_AddIntConstant(module, #constant, constant)

/* helper to define the type object.
 *
 * This assumes quite a few things about different things being available and their name.
 * For example, if prefix = foo, then there should be a deallocation function called foo_dealloc.
 * See the macro itself for other examples.
 */
#define PYSHAPELIB_DEFINE_TYPE(object, prefix, name, doc) \
{ \
		PYSHAPELIB_HEAD_INIT(NULL, 0) \
		name,								/*tp_name*/ \
		sizeof(object),						/*tp_basicsize*/ \
		0,									/*tp_itemsize*/ \
		(destructor) prefix ## _dealloc,	/*tp_dealloc*/ \
		0,									/*tp_print*/ \
		0,									/*tp_getattr*/ \
		0,									/*tp_setattr*/ \
		0,									/*tp_compare*/ \
		(reprfunc) prefix ## _repr,			/*tp_repr*/ \
		0,									/*tp_as_number*/ \
		0,									/*tp_as_sequence*/ \
		0,									/*tp_as_mapping*/ \
		0,									/*tp_hash */ \
		0,									/*tp_call*/ \
		0,									/*tp_str*/ \
		0,									/*tp_getattro*/ \
		0,									/*tp_setattro*/ \
		0,									/*tp_as_buffer*/ \
		Py_TPFLAGS_DEFAULT,					/*tp_flags*/ \
		doc,								/* tp_doc */ \
		0,									/* tp_traverse */ \
		0,									/* tp_clear */ \
		0,									/* tp_richcompare */ \
		0,									/* tp_weaklistoffset */ \
		0,									/* tp_iter */ \
		0,									/* tp_iternext */ \
		prefix ## _methods,					/* tp_methods */ \
		0,									/* tp_members */ \
		prefix ## _getsetters,				/* tp_getset */ \
		0,									/* tp_base */ \
		0,									/* tp_dict */ \
		0,									/* tp_descr_get */ \
		0,									/* tp_descr_set */ \
		0,									/* tp_dictoffset */ \
		(initproc) prefix ## _init,			/* tp_init */ \
		0,									/* tp_alloc */ \
		prefix ## _new,						/* tp_new */ \
	} \
	/**/

/* helper to add type to module.
 * Does a bit of the tedious bookkeeping for us
 */
#define PYSHAPELIB_ADD_TYPE(type, name) \
	Py_TYPE(&type) = &PyType_Type; \
	if (PyType_Ready(&type) >= 0) \
	{ \
		Py_INCREF(&type); \
		PyModule_AddObject(module, name, (PyObject*)&type); \
	}

#define PYSHAPELIB_NO_DATA 0


/* helpers to setup the shapelib API hooks correctly
 */
#if PY_VERSION_HEX >=0x03010000
	double pyshapelib_atof(const char *nptr)
	{
		double result;
		while (*nptr && isspace(*nptr))
		{
			++nptr;
		}
		result = PyOS_string_to_double(nptr, NULL, NULL);
		if (result == -1 && PyErr_Occurred())
		{
			return 0;
		}
		return result;
	}
#	define PYSHAPELIB_ATOF pyshapelib_atof
#elif PY_VERSION_HEX >=0x02040000
#	define PYSHAPELIB_ATOF PyOS_ascii_atof
#else
#	define PYSHAPELIB_ATOF atof
#endif

#if defined(SHPAPI_UTF8_HOOKS) && defined(SHPAPI_WINDOWS)
#	define HAVE_UTF8_HOOKS 1
#	define PYSHAPELIB_FILENAME_ENCODING "utf-8"
#	define PYSHAPELIB_SETUPHOOKS(pHooks)\
		SASetupUtf8Hooks(pHooks);\
		(pHooks)->Atof = PYSHAPELIB_ATOF
#else
#	define HAVE_UTF8_HOOKS 0
#	define PYSHAPELIB_FILENAME_ENCODING Py_FileSystemDefaultEncoding
#	define PYSHAPELIB_SETUPHOOKS(pHooks)\
		SASetupDefaultHooks(pHooks);\
		(pHooks)->Atof = PYSHAPELIB_ATOF
#endif

#endif

